package ru.ohotnik.laboratory.api

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * @author aleksander okhonchenko <strunasa@mail.ru>
 * @since 18.06.2017.
 */
@Retention(RetentionPolicy.RUNTIME)
@interface Benchmark {
}
