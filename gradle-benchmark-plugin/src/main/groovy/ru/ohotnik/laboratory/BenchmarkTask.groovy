package ru.ohotnik.laboratory

import org.gradle.api.DefaultTask
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.TaskAction

import java.util.function.Consumer

/**
 * @author aleksander okhonchenko <strunasa@mail.ru>
 * @since 18.06.2017.
 */
class BenchmarkTask extends DefaultTask {

    @InputFiles
    FileCollection files

    @TaskAction
    void action() {
        files.forEach(new Consumer<File>() {
            @Override
            void accept(File file) {
                println
            }
        })
    }

}
