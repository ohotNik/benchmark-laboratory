package ru.ohotnik.laboratory

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.SourceSet
import org.gradle.internal.classloader.VisitableURLClassLoader
import org.reflections.ReflectionUtils
import org.reflections.scanners.SubTypesScanner
import org.reflections.util.ConfigurationBuilder
import ru.ohotnik.laboratory.api.Benchmark

/**
 * @author aleksander okhonchenko <strunasa@mail.ru>
 * @since 18.06.2017.
 */
class GradleBenchmarkPlugin implements Plugin<Project>{

    @InputFiles
    Iterable<File> classpath

    @Override
    void apply(Project project) {



        SourceSet mainSourceSet = project.sourceSets.main
        classpath = project.sourceSets.main.runtimeClasspath



        project.task('benchmark') {
            doLast {

                /*Reflections reflections = new Reflections("ru.ohotnik")

                Set<Class<? extends Object>> allClasses =
                        reflections.getSubTypesOf(Object.class)*/

//                println allClasses

//                println Instrumentation.getInitiatedClasses((ClassLoader)getClass().getClassLoader())
//                println Instrumentation.getAllLoadedClasses()

                /*mainSourceSet.allJava.forEach(new Consumer<File>() {
                    @Override
                    void accept(File file) {
                        println(file.name)
                    }
                })*/


                def config = ConfigurationBuilder.build("ru.ohotnik")

                SubTypesScanner scanner = new SubTypesScanner()
                scanner.setConfiguration(config)
                scanner.scan(Object.class)
                println scanner.filterResultsBy(ReflectionUtils.withAnnotation(Benchmark.class)).store

                println ReflectionUtils.withAnnotation(Benchmark.class)

                println VisitableURLClassLoader.Spec.toString()

                /*classpath.forEach(new Consumer<File>() {
                    @Override
                    void accept(File file) {
                        println(file)
                    }
                })*/

//                println("${project.rootProject.}")
//                println 'aaaa'
            }
        }
    }
}
